package net.sinawali.lingo.util;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TestWordUtil extends TestCase {
	
	private static Log log = LogFactory.getLog(TestWordUtil.class);
	private WordUtil wordUtil;

	protected void setUp() throws Exception {
		super.setUp();
		
		wordUtil = new WordUtil(5);
	}
	
	public void testGenerateWords() {
		String word = wordUtil.getRandomWord();
		assertTrue("Word is null!", word != null);
		log.debug("Generated word: "+word);
		String word2 = wordUtil.getRandomWord();
		assertTrue("Word is null!", word2 != null);
		log.debug("Generated word: "+word2);
		
		assertNotSame("Words are the same, not very random!", word, word2);
	}

}
