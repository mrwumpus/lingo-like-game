package net.sinawali.lingo.util;

import junit.framework.TestCase;

public class TestWordScore extends TestCase {

	private static String word1 = "stout";

	private static String word2 = "trees";
	
	private static String word3 = "store";
	
	private static String word4 = "stoop";
	
	private static String word5 = "touts";
	
	private static String word6 = "ttttt";

	public void testWordCompare() {
		LetterScore[] score = WordScore.compareWords(word1, word2);
		assertEquals("?XXX?", WordScore.getScoreString(score));
		
		 score = WordScore.compareWords(word1, word3);
		 assertEquals("OOOXX", WordScore.getScoreString(score));
		 
		 score = WordScore.compareWords(word1, word4);
		 assertEquals("OOOXX", WordScore.getScoreString(score));
		 
		 score = WordScore.compareWords(word1, word5);
		 assertEquals("?????", WordScore.getScoreString(score));
		 
		 score = WordScore.compareWords(word1, word6);
		 assertEquals("XOXXO", WordScore.getScoreString(score));
		 
	}

}
