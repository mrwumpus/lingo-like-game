/**
 * 
 */
package net.sinawali.lingo.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.text.AbstractDocument;

import net.sinawali.lingo.ui.action.ExitAction;
import net.sinawali.lingo.ui.action.NewGame;
import net.sinawali.lingo.ui.action.ResignAction;
import net.sinawali.lingo.util.LetterScore;
import net.sinawali.lingo.util.MaxLengthDocumentFilter;
import net.sinawali.lingo.util.WordScore;
import net.sinawali.lingo.util.WordUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Joseph
 * 
 */
public class LingoWindow extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 2546765497891431128L;

    private static Log log = LogFactory.getLog(LingoWindow.class);

    private JLabel lblMessage;

    private JTextField txtWord;

    private MaxLengthDocumentFilter filter;

    private Container lettersPanel;

    private String secretWord;

    private LetterLabel[] initialLine;

    private JPanel initialLinePanel;

    private boolean newGame = true;

    private boolean correctGuess = false;

    private int guessCount = 0;

    private static WordUtil wordUtil;

    private int wordSize;
    
    private JScrollPane scrollPane;
    
    private JPanel scrollPanel;

    // -- File Menu
    private JMenuBar menuBar;

    private JMenu mnuFile;

    private JMenu mnuNewGame;

    private JMenuItem mniNewFour;

    private JMenuItem mniNewFive;

    private JMenuItem mniNewSix;

    private JMenuItem mniNewSeven;

    private JMenuItem mniNewEight;

    // private JMenuItem mniHint;

    private JMenuItem mniResign;

    private Action actResign;

    private JMenuItem mniExit;

    private Action actExit;

    public LingoWindow() {
        this.setTitle("LINGO");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setJMenuBar(getMnuBar());

        JPanel contentPanel = new JPanel(new BorderLayout());

        contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));

        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.add(getLblMessage(), BorderLayout.NORTH);
        bottomPanel.add(getTxtWord(), BorderLayout.SOUTH);
        
        contentPanel.add(bottomPanel, BorderLayout.SOUTH);
        
        scrollPanel = new JPanel(new BorderLayout());
        scrollPanel.add(getLettersContainer(), BorderLayout.NORTH);
        
        scrollPane = getScrollPane();
        scrollPane.setViewportView(scrollPanel);

        contentPanel.add(scrollPane, BorderLayout.CENTER);

        this.setContentPane(contentPanel);

        this.pack();
        this.setSize(300, 500);
    }

    public void startGame(int wordSize) {

        if (wordUtil == null) {
            wordUtil = new WordUtil(wordSize);
        } else {
            wordUtil.setWordLength(wordSize);
        }

        this.getMaxLenFilter().setMaxLength(wordSize);

        this.wordSize = wordSize;
        this.newGame = true;
        this.correctGuess = false;
        this.guessCount = 0;
        this.getLettersContainer().removeAll();
        secretWord = wordUtil.getRandomWord();

        LetterLabel[] ltrs = this.getInitialLine();
        ltrs[0].setLetter(secretWord.charAt(0));
        ltrs[0].setLetterScore(LetterScore.CORRECT);

        this.setTitle("LINGO - " + wordSize + " letters");

        this.getLettersContainer().add(getInitialLinePanel());
        this.getTxtWord().setText("");

        this.getResignAction().setEnabled(true);

        this.setMessage("New game!");

        this.validate();
        this.repaint();
    }

    private LetterLabel[] getInitialLine() {
        if (this.initialLine == null
                || this.initialLine.length != this.wordSize) {
            initialLine = new LetterLabel[wordSize];
            for (int i = 0; i < wordSize; ++i) {
                LetterLabel label = new LetterLabel();

                initialLine[i] = label;
            }
        }

        return initialLine;
    }

    private JPanel getInitialLinePanel() {

        this.initialLinePanel = new JPanel();

        LetterLabel[] letters = this.getInitialLine();
				for(LetterLabel label : letters) {
            this.initialLinePanel.add(label);
				}

        return this.initialLinePanel;
    }
    
    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setWheelScrollingEnabled(true);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        }

        return scrollPane;
    }

    private JTextField getTxtWord() {
        if (this.txtWord == null) {
            this.txtWord = new JTextField();
            ((AbstractDocument) this.txtWord.getDocument()).setDocumentFilter(this.getMaxLenFilter());

            this.txtWord.addActionListener(e -> {
									if (!correctGuess) {
											setGuess(getTxtWord().getText());
									} else {
											Toolkit.getDefaultToolkit().beep();
									}
							});
        }

        return this.txtWord;
    }

    private MaxLengthDocumentFilter getMaxLenFilter() {
        if (filter == null) {
            filter = new MaxLengthDocumentFilter(0);
        }

        return filter;
    }

    private JLabel getLblMessage() {
        if (lblMessage == null) {
            lblMessage = new JLabel();
            lblMessage.setHorizontalAlignment(SwingConstants.CENTER);
        }

        return lblMessage;
    }

    private void setMessage(String message) {
        JLabel label = getLblMessage();
        label.setText(message);
        label.repaint();
    }

    private Container getLettersContainer() {
        if (this.lettersPanel == null) {
            this.lettersPanel = Box.createVerticalBox();
        }

        return this.lettersPanel;
    }

    private LetterLabel[] getLetters() {

        LetterLabel[] letters = new LetterLabel[wordSize];

        for (int i = 0; i < wordSize; ++i) {
            LetterLabel label = new LetterLabel();
            letters[i] = label;
        }

        return letters;
    }

    private void setGuess(String guess) {
        if (guess.length() != wordSize) {
            this.setMessage("Please enter a " + wordSize + " letter word.");
            return;
        }

        this.getTxtWord().setSelectionStart(0);
        this.getTxtWord().setSelectionEnd(wordSize);

        int guessScore = 0;

        guessCount++;

        LetterScore[] score;
        if (!wordUtil.isWordInList(guess)) {
            score = new LetterScore[wordSize];
            for (int i = 0; i < score.length; i++) {
                score[i] = LetterScore.INCORRECT;
            }
            this.setMessage("Word not found!");
            guessScore = -1;
        } else {
            score = WordScore.compareWords(this.secretWord, guess);
        }
        LetterLabel[] ltrs = this.getLetters();

        this.correctGuess = true;

        for (int i = 0; i < score.length; i++) {
            LetterScore letterScore = score[i];

            ltrs[i].setLetter(guess.charAt(i));
            ltrs[i].setLetterScore(letterScore);

            guessScore += getIntScoreForScore(letterScore);

            this.correctGuess = this.correctGuess
                    && letterScore == LetterScore.CORRECT;
        }

        if (guessScore >= 0) {
            this.setMessage(getMessageForScore(guessScore));
        }

        if (this.correctGuess) {
            this.getResignAction().setEnabled(false);
        }
        
        this.addLettersLine(ltrs);
    }

    public void giveUp() {
        this.setGuess(this.secretWord);
        this.setMessage("Here's the correct answer.");
    }

    private int getIntScoreForScore(LetterScore score) {
        if (score == LetterScore.CORRECT) {
            return 2;
        }

        if (score == LetterScore.WRONG_PLACE) {
            return 1;
        }

        return 0;
    }

    private String getMessageForScore(int score) {
        if (score == wordSize * 2) {
            return "You guessed it in " + this.guessCount
                    + " tries! Brilliant!";
        }

        if (score > wordSize + wordSize / 2) {
            return "Fantastic guess!!!";
        }

        if (score > wordSize) {
            return "Great guess!";
        }

        if (score > wordSize / 2 + 1) {
            return "Good guess.";
        }

        return "You can do better than that!";

    }

    private void addLettersLine(LetterLabel[] labels) {
        JPanel lettersPanel = new JPanel();

				for(LetterLabel label : labels) {
            lettersPanel.add(label);
        }

        if (this.newGame) {
            this.getLettersContainer().remove(getInitialLinePanel());
            this.newGame = false;
        }

        this.getLettersContainer().add(lettersPanel);

        this.validate();
        
        lettersPanel.scrollRectToVisible(lettersPanel.getBounds());
    }

    private JMenuBar getMnuBar() {
        if (menuBar == null) {
            menuBar = new JMenuBar();
            menuBar.add(getMnuFile());
        }

        return menuBar;
    }

    private JMenu getMnuFile() {
        if (mnuFile == null) {
            mnuFile = new JMenu("File");
            mnuFile.setMnemonic(KeyEvent.VK_F);
            mnuFile.add(getMnuNewGame());
            mnuFile.add(getMniResign());
            mnuFile.addSeparator();
            mnuFile.add(getMniExit());
        }

        return mnuFile;
    }

    private JMenu getMnuNewGame() {
        if (mnuNewGame == null) {
            mnuNewGame = new JMenu("New Game");
            mnuNewGame.setMnemonic(KeyEvent.VK_N);
            mnuNewGame.add(getMniNewFour());
            mnuNewGame.add(getMniNewFive());
            mnuNewGame.add(getMniNewSix());
            mnuNewGame.add(getMniNewSeven());
            mnuNewGame.add(getMniNewEight());
        }

        return mnuNewGame;
    }

    private JMenuItem getMniNewFour() {
        if (mniNewFour == null) {
            mniNewFour = new JMenuItem();
            mniNewFour.setAction(new NewGame(this, 4));
        }

        return mniNewFour;
    }

    private JMenuItem getMniNewFive() {
        if (mniNewFive == null) {
            mniNewFive = new JMenuItem();
            mniNewFive.setAction(new NewGame(this, 5));
        }

        return mniNewFive;
    }

    private JMenuItem getMniNewSix() {
        if (mniNewSix == null) {
            mniNewSix = new JMenuItem();
            mniNewSix.setAction(new NewGame(this, 6));
        }

        return mniNewSix;
    }

    private JMenuItem getMniNewSeven() {
        if (mniNewSeven == null) {
            mniNewSeven = new JMenuItem();
            mniNewSeven.setAction(new NewGame(this, 7));
        }

        return mniNewSeven;
    }

    private JMenuItem getMniNewEight() {
        if (mniNewEight == null) {
            mniNewEight = new JMenuItem();
            mniNewEight.setAction(new NewGame(this, 8));
        }

        return mniNewEight;
    }

    private JMenuItem getMniResign() {
        if (mniResign == null) {
            mniResign = new JMenuItem();
            mniResign.setAction(getResignAction());
        }

        return mniResign;
    }

    private Action getResignAction() {
        if (actResign == null) {
            actResign = new ResignAction(this);
            actResign.setEnabled(false);
        }

        return actResign;

    }

    private JMenuItem getMniExit() {
        if (mniExit == null) {
            mniExit = new JMenuItem();
            mniExit.setAction(getActExit());
        }

        return mniExit;
    }

    private Action getActExit() {
        if (actExit == null) {
            actExit = new ExitAction(this);
        }

        return actExit;
    }

}
