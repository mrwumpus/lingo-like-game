/**
 * 
 */
package net.sinawali.lingo.ui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.sinawali.lingo.util.LetterScore;

/**
 * @author Joseph
 * 
 */
public class LetterLabel extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5066309957484142784L;

	private LetterScore score = null;

	private static final Color correct_color = Color.orange;

	private static final Color wrong_color = Color.lightGray;

	private static final Color position_color = Color.yellow;

	public LetterLabel() {
		super();

		this.setOpaque(true);
		this.setPreferredSize(new Dimension(20, 20));
		this.setHorizontalAlignment(SwingConstants.CENTER);
	}

	public void setLetter(char letter) {
		this.setText(String.valueOf(letter));
	}

	public void setLetterScore(LetterScore score) {
		this.score = score;
		this.updateScore();
		this.repaint();
	}

	private void updateScore() {
		Color bgColor = wrong_color;

		switch(score) {
			case CORRECT:
				bgColor = correct_color;
				break;
			case WRONG_PLACE:
				bgColor = position_color;
				break;
		}
			
		this.setBackground(bgColor);
	}

}
