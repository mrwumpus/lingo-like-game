/**
 * 
 */
package net.sinawali.lingo.ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.sinawali.lingo.ui.LingoWindow;

/**
 * @author Joe
 * 
 */
public class ResignAction extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = 6310447849224942160L;

    private LingoWindow window;

    public ResignAction(LingoWindow window) {
        super();
        this.window = window;

        this.putValue(Action.NAME, "Give up");
        this.putValue(Action.SHORT_DESCRIPTION, "Give up on this word.");
        this.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_G);
        this.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                KeyEvent.VK_G, KeyEvent.CTRL_DOWN_MASK));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0) {
        this.window.giveUp();
    }

}
