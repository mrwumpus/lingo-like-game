package net.sinawali.lingo.ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

public class ExitAction extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = 6256453644455852400L;

    private JFrame frame;

    public ExitAction(JFrame frame) {
        super();

        this.frame = frame;

        this.putValue(Action.NAME, "Exit");
        this.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
        this.putValue(Action.SHORT_DESCRIPTION, "Exit Lingo");
        this.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
    }

    public void actionPerformed(ActionEvent e) {
        frame.setVisible(false);
				frame.dispose();
    }

}
