/**
 * 
 */
package net.sinawali.lingo.ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import net.sinawali.lingo.ui.LingoWindow;

/**
 * @author Joe
 * 
 */
public class NewGame extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = 3072798656735186058L;

    private int numLetters;

    private LingoWindow lingo;

    public NewGame(LingoWindow window, int letters) {
        super();

        this.lingo = window;
        this.numLetters = letters;

        this.putValue(Action.NAME, letters + " letters");
        this.putValue(Action.SHORT_DESCRIPTION, "Guess a word with " + letters
                + " letters");
        int keyEvent = KeyEvent.VK_5;

        switch (letters) {
        case 4:
            keyEvent = KeyEvent.VK_4;
            break;
        case 5:
            keyEvent = KeyEvent.VK_5;
            break;
        case 6:
            keyEvent = KeyEvent.VK_6;
            break;
        case 7:
            keyEvent = KeyEvent.VK_7;
            break;
        case 8:
            keyEvent = KeyEvent.VK_8;
            break;
        }
        this.putValue(Action.MNEMONIC_KEY, keyEvent);

        this.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(keyEvent,
                KeyEvent.CTRL_MASK));
    }

    public void actionPerformed(ActionEvent arg0) {

        this.lingo.startGame(this.numLetters);

    }

}
