/**
 * 
 */
package net.sinawali.lingo.util;

/**
 * @author Joseph
 *
 */
public enum LetterScore {
	CORRECT, WRONG_PLACE, INCORRECT;
}
