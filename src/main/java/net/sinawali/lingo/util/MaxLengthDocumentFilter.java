package net.sinawali.lingo.util;

import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/*
 * Created on Oct 18, 2005
 */

/**
 * Confines the length of the JTextComponents' document. Made to replace the
 * MaxLengthDocument.<br>
 * Use as such:<br>
 * <code>
 * int MAX_LEN = 255;
 * 	...
 * JTextComponent textComp = ...
 * ((AbstractDocument)textComp.getDocument()).setDocumentFilter(new MaxLengthDocumentFilter(MAX_LEN));
 * </code>
 * 
 * @author jfox
 */
public class MaxLengthDocumentFilter extends DocumentFilter {

//    private static Log log = LogFactory.getLog(MaxLengthDocumentFilter.class);

    private int maxDocumentLength;

    /**
     * Constructs the Filter to limit the size of the document to
     * maxDocumentLength characters.
     * 
     * @param maxDocumentLength
     */
    public MaxLengthDocumentFilter(int maxDocumentLength) {
        super();
        this.maxDocumentLength = maxDocumentLength;
    }
    
    /**
     * @param length
     */
    public void setMaxLength(int length) {
        this.maxDocumentLength = length;
    }

    /*
     * @see javax.swing.text.DocumentFilter#insertString(javax.swing.text.DocumentFilter.FilterBypass,
     *      int, java.lang.String, javax.swing.text.AttributeSet)
     */
    public void insertString(FilterBypass fb, int offset, String str,
            AttributeSet attr) throws BadLocationException {

        if (str == null
                || (fb.getDocument().getLength() + str.length()) <= maxDocumentLength) {
            super.insertString(fb, offset, str, attr);

        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    /*
     * @see javax.swing.text.DocumentFilter#replace(javax.swing.text.DocumentFilter.FilterBypass,
     *      int, int, java.lang.String, javax.swing.text.AttributeSet)
     */
    public void replace(FilterBypass fb, int offset, int length, String str,
            AttributeSet attrs) throws BadLocationException {

        // This rejects the entire replacement if it would make
        // the contents too long. Another option would be
        // to truncate the replacement string so the contents
        // would be exactly maxDocumentLength in length.
        if (str == null
                || (fb.getDocument().getLength() + str.length() - length) <= maxDocumentLength) {
            super.replace(fb, offset, length, str, attrs);

        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

}
