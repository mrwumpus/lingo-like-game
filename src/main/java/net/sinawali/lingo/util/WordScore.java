/**
 * 
 */
package net.sinawali.lingo.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Joseph
 * 
 */
public class WordScore {

	private static Log log = LogFactory.getLog(WordScore.class);

	public static LetterScore[] compareWords(String rightWord, String guess) {
		if (rightWord.length() != guess.length()) {
			throw new IllegalArgumentException("Strings different length");
		}

		List<String> wordList = new ArrayList<String>();
		for (int i = 0; i < rightWord.length(); ++i) {
			wordList.add(rightWord.substring(i, i + 1));
		}

		LetterScore[] retScores = new LetterScore[rightWord.length()];

		// Determine matches first...
		for (int i = 0; i < guess.length(); ++i) {
			char letter = guess.charAt(i);

			if (letter == rightWord.charAt(i)) {
				retScores[i] = LetterScore.CORRECT;
				wordList.set(i, "#");
			}
		}

		log.debug("After matches: " + getScoreString(retScores));

		// Then determine wrong_places and incorrects...
		for (int i = 0; i < guess.length(); ++i) {
			// Skip it if it's correct.
			if( retScores[i] == LetterScore.CORRECT) {
				continue;
			}
			char letter = guess.charAt(i);
			int idx = -1;
			if ((idx = wordList.indexOf(String.valueOf(letter))) > -1) {
				retScores[i] = LetterScore.WRONG_PLACE;
				wordList.set(idx, "#");
			} else {
					retScores[i] = LetterScore.INCORRECT;
			}
		}

		return retScores;
	}

	public static String getScoreString(LetterScore[] score) {
		StringBuilder b = new StringBuilder();

		for (int i = 0; i < score.length; i++) {
			LetterScore letterScore = score[i];

			b.append(getLetterScoreString(letterScore));
		}

		return b.toString();

	}

	private static String getLetterScoreString(LetterScore score) {
		if (score == LetterScore.CORRECT) {
			return "O";
		}

		if (score == LetterScore.INCORRECT) {
			return "X";
		}

		if (score == LetterScore.WRONG_PLACE) {
			return "?";
		}

		return "!";
	}

}
