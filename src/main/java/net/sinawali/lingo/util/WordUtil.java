package net.sinawali.lingo.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Joseph
 * 
 */
public class WordUtil {

    private static Log log = LogFactory.getLog(WordUtil.class);

    private List<String> wordList = new LinkedList<String>();

    private static SecureRandom rand;

    static {
        try {
            rand = SecureRandom.getInstance("NativePRNG");
            rand.setSeed(Calendar.getInstance().getTimeInMillis());
        } catch (NoSuchAlgorithmException ex) {
						throw new RuntimeException(ex);
        }
    }

    public WordUtil(int wordLength) {
        super();

        this.initialize(wordLength);
    }

    public void setWordLength(int wordLength) {
        this.initialize(wordLength);
    }

    private void initialize(int wordLength) {
        if (wordLength > 8 || wordLength < 4) {
            throw new IllegalArgumentException(
                    "Word length must be between 4 and 8.");
        }
        
        wordList.clear();

        String wordFile = new StringBuilder().append(wordLength).append(
                "letterWords.txt").toString();
        InputStream is = Class.class.getResourceAsStream(new StringBuilder("/").append(
                wordFile).toString());

        if (is == null) {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    wordFile);
        }

        if (is != null) {
						try(BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is))) {

                String line;
                while ((line = reader.readLine()) != null) {
                    String[] words = line.split(" ");
                    wordList.addAll(Arrays.asList(words));
                }

            } catch (IOException e) {
							throw new RuntimeException(e);
            } 

            log.debug("Loaded " + wordList.size() + " words!");
        } else {
					throw new RuntimeException("Unable to load wordlist for " + wordFile);
				}
    }

    /**
     * @return Will return null if initialization failed.
     */
    public String getRandomWord() {
        if (rand == null || wordList.size() == 0) {
            return null;
        }

        int wordIndex = Math.abs(rand.nextInt()) % wordList.size();

        log.debug("Returning word number " + wordIndex);
        return wordList.get(wordIndex);
    }

    public boolean isWordInList(String word) {
        return wordList.contains(word);
    }

}
