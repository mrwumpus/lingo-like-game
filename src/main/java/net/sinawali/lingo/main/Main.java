/**
 * 
 */
package net.sinawali.lingo.main;

import javax.swing.SwingUtilities;

import net.sinawali.lingo.ui.LingoWindow;

/**
 * @author Joseph
 * 
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(() -> {
				LingoWindow window = new LingoWindow();
				window.setVisible(true);
		});

	}

}
